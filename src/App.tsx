import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ThemeProvider } from '@mui/material';
import { Selectors } from './store';
import { getData, choiseTheme } from './service';
import { HomePage } from './pages';
import { GlobalStylesAllPages, defaultTheme, ThemeColors } from './styled';

export const App = () => {
  const dispatch = useDispatch();
  const checkTheme = useSelector(Selectors.themeValue);
  const theme = defaultTheme(choiseTheme(checkTheme, ThemeColors));

  useEffect(() => {
    dispatch(getData());
  }, [dispatch]);

  return (
    <ThemeProvider theme={theme}>
      <GlobalStylesAllPages background={theme.palette.primary.dark} />
      <div className="App">
        <HomePage />
      </div>
    </ThemeProvider>
  );
};
