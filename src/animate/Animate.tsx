import React, { FC, ReactNode } from 'react';
import { motion, AnimatePresence } from 'framer-motion';

interface IAnimate {
  children: ReactNode;
}

export const SpawnAnimation: FC<IAnimate> = ({ children }) => {
  return (
    <AnimatePresence>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
      >
        {children}
      </motion.div>
    </AnimatePresence>
  );
};

export const MountAnimation: FC<IAnimate> = ({ children }) => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      {children}
    </motion.div>
  );
};
