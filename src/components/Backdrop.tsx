import React, { FC } from 'react';
import Backdrop from '@mui/material/Backdrop';
import { Loader } from './Loader';

interface IBackdrop {
  boolean: boolean;
}

export const SimpleBackdrop: FC<IBackdrop> = ({ boolean }) => {
  return (
    <Backdrop
      sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
      open={boolean}
    >
      <Loader />
    </Backdrop>
  );
};
