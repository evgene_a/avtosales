import React, { FC } from 'react';
import Button, { ButtonProps } from '@mui/material/Button';

export const ButtonPrimary: FC<ButtonProps> = ({
  children,
  onClick,
  disabled = false,
  ...props
}: ButtonProps) => {
  return (
    <Button {...props} onClick={onClick} disabled={disabled}>
      {children}
    </Button>
  );
};
