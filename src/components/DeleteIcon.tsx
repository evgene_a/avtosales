import React, { FC } from 'react';
import DeleteIcon from '@mui/icons-material/Delete';
import { IconButton, Tooltip } from '@mui/material';
import { useDispatch } from 'react-redux';
import { deleteData } from '../service';

interface IDelete {
  id: string;
  right?: string;
  top?: string;
}

export const DeleteIconItem: FC<IDelete> = ({ id, right = '0', top = '0' }) => {
  const dispatch = useDispatch();
  const handleClick = (id: string): void => {
    dispatch(deleteData(id));
  };

  return (
    <Tooltip title="Удалить" sx={{ position: 'absolute', right, top }}>
      <IconButton onClick={() => handleClick(id)}>
        <DeleteIcon />
      </IconButton>
    </Tooltip>
  );
};
