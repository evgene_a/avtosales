import React, { FC } from 'react';
import EditIcon from '@mui/icons-material/Edit';
import { IconButton, Tooltip } from '@mui/material';

interface IEditIcon {
  right?: string;
  clickOn?: any;
  children?: React.ReactNode;
  top?: string;
}

export const EditIconItem: FC<IEditIcon> = ({
  right = '0',
  clickOn,
  children,
  top = '0',
}) => {
  return (
    <Tooltip title="Редактировать" sx={{ position: 'absolute', right, top }}>
      <IconButton onClick={() => clickOn(true)}>
        {children}
        <EditIcon />
      </IconButton>
    </Tooltip>
  );
};
