import React, { FC, ReactNode } from 'react';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import { useTheme } from '@mui/material/styles';
import { HeadingTitle } from './Heading';
import { SearchInput } from './Search';
import { SFooter } from '../styled';

interface IFooter {
  children?: ReactNode;
}

export const Footer: FC<IFooter> = ({ children }) => {
  const theme = useTheme();
  return (
    <Container maxWidth="lg" sx={{ flexGrow: 1 }}>
      <SFooter>
        <Paper
          elevation={0}
          sx={{
            borderRadius: '10px 10px 0 0',
            height: 'auto',
            padding: '25px 40px 20px 40px',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            flexWrap: 'wrap',
          }}
        >
          <HeadingTitle
            text="©&nbsp;2022, AutoSales"
            tag="h5"
            color={theme.palette.success.light}
          />
          <SearchInput placeholder="Поиск..." />
          {children}
        </Paper>
      </SFooter>
    </Container>
  );
};
