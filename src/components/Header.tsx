import React, { FC } from 'react';
import { AppBar, Toolbar, Container } from '@mui/material';
import { MESSAGES } from '../constants';
import { HeadingTitle } from './Heading';
import { SwitchLabels } from './Switch';

export const Header: FC = () => {
  return (
    <Container maxWidth="lg" sx={{ flexGrow: 1 }}>
      <AppBar position="static" sx={{ borderRadius: '0 0 10px 10px' }}>
        <Toolbar
          variant="dense"
          sx={{
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'space-between',
          }}
        >
          <HeadingTitle text={MESSAGES.header.headerTitle} />
          <SwitchLabels />
        </Toolbar>
      </AppBar>
    </Container>
  );
};
