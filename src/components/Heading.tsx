import React, { FC } from 'react';

interface IHeadingTitle {
  tag?: string | any;
  text: string;
  color?: string;
}
export const HeadingTitle: FC<IHeadingTitle> = ({ tag, text, color }) => {
  const Tag = tag || 'h1';
  return <Tag style={{ color }}>{text}</Tag>;
};
