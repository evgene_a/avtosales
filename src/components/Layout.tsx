import React, { FC, ReactNode } from 'react';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import { useTheme } from '@mui/material/styles';
import { useSelector } from 'react-redux';
import { Header } from './Header';
import { Footer } from './Footer';
import { Snackbars } from './Snackbar';
import { SLayout } from '../styled/SLayout';
import { SimpleBackdrop } from './Backdrop';
import { loaderCheck, loadError } from '../store/selectors';

export interface ILayout {
  children: ReactNode;
}

export const Layout: FC<ILayout> = ({ children }) => {
  const theme = useTheme();
  const loader = useSelector(loaderCheck);
  const errorType = useSelector(loadError).split(':');
  return (
    <SLayout>
      <Header />
      <main>
        <Container maxWidth="lg">
          <Paper
            elevation={0}
            sx={{
              padding: '30px 30px 70px 30px',
              borderRadius: '10px',
              backgroundColor: theme.palette.success.dark,
            }}
          >
            {children}
          </Paper>
        </Container>
      </main>
      <Footer />
      {loader && <SimpleBackdrop boolean={loader} />}
      {errorType[1] && <Snackbars errorMessage={errorType[1]} />}
    </SLayout>
  );
};
