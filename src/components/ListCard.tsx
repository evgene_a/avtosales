/* eslint-disable import/no-cycle */
import { Paper, useTheme } from '@mui/material';
import React, { FC, useState } from 'react';

import { DeleteIconItem } from './DeleteIcon';
import { FormDialog } from './ModalDialog';
import { SpawnAnimation } from '../animate';
import { EditIconItem } from './EditIcon';
import {
  CardUserQuestions,
  BlockUserData,
  UserName,
  UserdataAuto,
  BlockText,
  Time,
} from '../styled';

export interface IListCard {
  item: {
    email: string;
    text: string;
    carModel: string;
    carBrand: string;
    name: string;
    time: string;
    id: string;
  };
}

export const ListCard: FC<IListCard> = React.memo(({ item }) => {
  const theme = useTheme();
  const { carModel, carBrand, name, time, text, id } = item;
  const [open, setOpen] = useState<boolean>(false);

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <SpawnAnimation>
      <Paper
        elevation={0}
        sx={{
          borderRadius: '10px',
          padding: '30px',
          margin: '15px',
          backgroundColor: theme.palette.primary.dark,
        }}
      >
        <CardUserQuestions>
          <EditIconItem right="45px" top="-15px" clickOn={handleOpen} />
          <DeleteIconItem top="-15px" id={id} />
          <BlockUserData>
            <UserName>{name}</UserName>
            <UserdataAuto>
              <div>{carBrand}</div>
              <div>{carModel}</div>
            </UserdataAuto>
          </BlockUserData>
          <BlockText>
            <Time>{time}</Time>
            <div>{text}</div>
          </BlockText>
        </CardUserQuestions>
        {open && (
          <FormDialog data={item} openModal={open} handleClose={handleClose} />
        )}
      </Paper>
    </SpawnAnimation>
  );
});
