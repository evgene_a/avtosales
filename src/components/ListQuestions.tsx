/* eslint-disable import/no-cycle */
import React, { FC } from 'react';
import { SListQuestions } from '../styled';
import { ListCard } from './ListCard';
import { MESSAGES } from '../constants';
import { HeadingTitle } from './Heading';

export interface IData {
  data: Array<{
    id: string;
    time: string;
    email: string;
    text: string;
    carModel: string;
    carBrand: string;
    name: string;
  }>;
}
export const ListQuestions: FC<IData> = ({ data }) => {
  return (
    <SListQuestions>
      <div>
        <ul>
          {data.length ? (
            data.map((item) => <ListCard key={item.id} item={item} />)
          ) : (
            <HeadingTitle text={MESSAGES.ListQuestion} tag="h3" />
          )}
        </ul>
      </div>
    </SListQuestions>
  );
};
