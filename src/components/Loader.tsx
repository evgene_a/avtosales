import React, { FC } from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

type CircularProps = {
  size?: number;
};

export const Loader: FC<CircularProps> = ({ size }) => {
  return (
    <Box sx={{ display: 'flex', justifyContent: 'center', padding: '20px' }}>
      <CircularProgress sx={{ color: '#fff' }} size={size} />
    </Box>
  );
};
