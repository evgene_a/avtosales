/* eslint-disable import/no-cycle */
import React, { FC } from 'react';
import { DialogContent, IconButton } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { useTheme } from '@mui/material/styles';
import { QuestionForm } from '../forms';
import { Content } from '../styled';
import { UserMessage } from '../store';

interface IFormDialog {
  data?: UserMessage;
  openModal: boolean;
  handleClose: () => void;
}

export const FormDialog: FC<IFormDialog> = ({
  data,
  openModal,
  handleClose,
}) => {
  const theme = useTheme();
  return (
    <div>
      <Content open={openModal} onClose={() => handleClose()}>
        <DialogContent
          sx={{
            padding: '30px 30px 65px 20px',
            backgroundColor: theme.palette.success.dark,
          }}
        >
          <IconButton
            aria-label="Закрыть"
            onClick={() => handleClose()}
            sx={{
              position: 'absolute',
              right: 15,
              top: 10,
            }}
          >
            <CloseIcon />
          </IconButton>
          <QuestionForm
            clickOff={() => handleClose()}
            text="Редактирование"
            userItem={data}
          />
        </DialogContent>
      </Content>
    </div>
  );
};
