import React, { FC } from 'react';
import SearchIcon from '@mui/icons-material/Search';
import ClearIcon from '@mui/icons-material/Clear';
import { useDispatch, useSelector } from 'react-redux';
import { searchMessage } from '../store/selectors';
import { searchUserMessages } from '../store';
import { ButtonPrimary } from './ButtonPrimary';
import { Search, SearchIconWrapper, StyledInputBase } from '../styled';
import { MountAnimation } from '../animate';

interface ISearchInput {
  placeholder?: string;
}

export const SearchInput: FC<ISearchInput> = React.memo(({ placeholder }) => {
  const dispatch = useDispatch();
  const searchData = useSelector(searchMessage);

  return (
    <Search>
      <ButtonPrimary
        onClick={() => dispatch(searchUserMessages(''))}
        style={{ padding: 0 }}
        disabled={!searchData}
      >
        <SearchIconWrapper>
          {searchData ? (
            <MountAnimation>
              <ClearIcon style={{ paddingTop: '8px' }} />
            </MountAnimation>
          ) : (
            <SearchIcon />
          )}
        </SearchIconWrapper>
      </ButtonPrimary>
      <StyledInputBase
        placeholder={placeholder}
        value={searchData}
        onChange={(event) => dispatch(searchUserMessages(event.target.value))}
      />
    </Search>
  );
});
