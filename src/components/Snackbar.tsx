import React, { FC } from 'react';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import { useDispatch } from 'react-redux';
import { dataError } from '../store';

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
  props,
  ref
) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

interface ISnackbars {
  errorMessage?: string | undefined;
}

export const Snackbars: FC<ISnackbars> = ({ errorMessage }) => {
  const dispatch = useDispatch();
  const handleClose = (
    event?: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === 'clickaway') {
      return dispatch(dataError(''));
    }
    dispatch(dataError(''));
  };

  return (
    <Snackbar
      open={Boolean(errorMessage)}
      autoHideDuration={4000}
      onClose={handleClose}
    >
      <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
        Ошибка: {errorMessage}
      </Alert>
    </Snackbar>
  );
};
