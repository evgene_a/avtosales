import React, { FC } from 'react';
import { FormGroup, FormControlLabel, Switch } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { themeCheked } from '../store';
import { themeValue } from '../store/selectors';

export const SwitchLabels: FC = React.memo(() => {
  const theme = useSelector(themeValue);
  const dispatch = useDispatch();
  const hendleChange: React.ChangeEventHandler<HTMLInputElement> = (event) => {
    dispatch(themeCheked(event.target.checked));
  };
  return (
    <FormGroup onChange={hendleChange}>
      <FormControlLabel
        control={<Switch value={theme} color="warning" />}
        label="Изменить тему"
      />
    </FormGroup>
  );
});
