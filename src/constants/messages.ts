export const MESSAGES = {
  locales: 'ru-RU',
  header: {
    headerTitle: 'Вопросы по автомобилям',
  },
  questionForm: {
    title: 'Задать вопрос',
    nameForm: {
      placeholder: 'Имя',
      error: 'Ошибка при вводе имени',
    },
    emailInput: {
      placeholder: 'Email',
      error: 'Ошибка при вводе email',
    },
    carBrandInput: {
      placeholder: 'Марка авто',
      error: 'Ошибка при вводе марки',
    },
    carModelInput: {
      placeholder: 'Модель авто',
      error: 'Ошибка при вводе модели',
    },
    textareaInput: {
      placeholder: 'Введите текст вопроса',
      error: 'Ошибка ввода текста в поле',
    },
    sendButtonText: 'ОТПРАВИТЬ',
    pattern: {
      patternEmail:
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      patternName: /^[А-Яа-яЁё]+$/,
      patternCar: /^[A-Za-z0-9\s]+$/,
      patternTextArea: /^[A-Za-zА-Яа-яЁё0-9.,()!?\-\s]+$/,
    },
  },
  ListQuestion: 'None',
};
