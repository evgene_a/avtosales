import { MESSAGES } from './messages';

export type TypeMessagesHeader = typeof MESSAGES.header.headerTitle;
export type TypeMessagesQuestionForm = typeof MESSAGES.questionForm;
