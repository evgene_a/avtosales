/* eslint-disable import/no-cycle */
import React, { FC } from 'react';
import { useForm, SubmitHandler } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import {
  Divider,
  Chip,
  TextField,
  useTheme,
  TextareaAutosize,
} from '@mui/material';
import { SQuestionForm, InputsBlock, Button } from '../styled';
import { ButtonPrimary } from '../components';
import { MESSAGES } from '../constants';
import { UserMessage } from '../store';
import { postData, putData, newId } from '../service';

interface IFormInput {
  email: string;
  text: string;
  carModel: string;
  carBrand: string;
  name: string;
}

interface IQuestionForm {
  clickOff?: (arg0: boolean) => void;
  text?: string;
  userItem?: UserMessage;
}

export const QuestionForm: FC<IQuestionForm> = ({
  clickOff,
  text,
  userItem,
}) => {
  const dispatch = useDispatch();
  const {
    title,
    nameForm,
    emailInput,
    carBrandInput,
    carModelInput,
    textareaInput,
    sendButtonText,
    pattern,
  } = MESSAGES.questionForm;
  const theme = useTheme();

  const {
    register,
    handleSubmit,
    reset,
    getValues,
    formState: { errors },
  } = useForm<IFormInput>({ mode: 'onTouched', shouldFocusError: false });

  const newDateString = () => {
    const nowDate = new Date();
    return `${
      userItem ? `Отредактировано ${getValues().name}, ` : ''
    } ${nowDate.toLocaleDateString()} в ${nowDate.toLocaleTimeString(
      MESSAGES.locales,
      { hour: 'numeric', minute: 'numeric' }
    )}`;
  };

  const onSubmit: SubmitHandler<IFormInput> = (data) => {
    const userData = {
      ...data,
      time: newDateString(),
      id: userItem ? userItem.id : newId(),
    };
    reset();
    if (clickOff) {
      clickOff(false);
      return dispatch(putData(userData));
    }
    return dispatch(postData(userData));
  };

  return (
    <SQuestionForm
      textcolor={theme.palette.success.main}
      backgroundtextarea={theme.palette.success.dark}
      areaborder={theme.palette.warning.light}
    >
      <Divider sx={{ margin: '15px 0' }}>
        <Chip label={text || title} />
      </Divider>
      <div>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div>
            <InputsBlock>
              <div>
                <TextField
                  type="text"
                  defaultValue={userItem?.name}
                  error={!!errors.name}
                  data-testid="input-name"
                  placeholder={nameForm.placeholder}
                  {...register('name', {
                    required: true,
                    pattern: pattern.patternName,
                    maxLength: 30,
                    minLength: 2,
                  })}
                />
                {errors.name && <p>{nameForm.error}</p>}
              </div>
              <div>
                <TextField
                  defaultValue={userItem?.email}
                  error={!!errors.email}
                  data-testid="email"
                  placeholder={emailInput.placeholder}
                  {...register('email', {
                    required: true,
                    pattern: pattern.patternEmail,
                  })}
                />
                {errors.email && <p>{emailInput.error}</p>}
              </div>
            </InputsBlock>
            <InputsBlock>
              <div>
                <TextField
                  defaultValue={userItem?.carBrand}
                  error={!!errors.carBrand}
                  placeholder={carBrandInput.placeholder}
                  {...register('carBrand', {
                    required: true,
                    pattern: pattern.patternCar,
                    maxLength: 20,
                    minLength: 2,
                  })}
                />
                {errors.carBrand && <p>{carBrandInput.error}</p>}
              </div>
              <div>
                <TextField
                  defaultValue={userItem?.carModel}
                  error={!!errors.carModel}
                  placeholder={carModelInput.placeholder}
                  {...register('carModel', {
                    required: true,
                    pattern: pattern.patternCar,
                    maxLength: 20,
                    minLength: 2,
                  })}
                />
                {errors.carModel && <p>{carModelInput.error}</p>}
              </div>
            </InputsBlock>
          </div>
          <TextareaAutosize
            minRows={5}
            aria-label="maximum height"
            defaultValue={userItem?.text}
            placeholder={textareaInput.placeholder}
            style={errors.text && { border: '1px solid red' }}
            {...register('text', {
              required: true,
              pattern: pattern.patternTextArea,
              minLength: 5,
              maxLength: 500,
            })}
          />
          {errors.text && <p>{textareaInput.error}</p>}
          <Button buttonhover={theme.palette.warning.main}>
            <ButtonPrimary
              data-testid="button"
              type="submit"
              variant="contained"
              color="success"
            >
              {sendButtonText}
            </ButtonPrimary>
          </Button>
        </form>
      </div>
    </SQuestionForm>
  );
};
