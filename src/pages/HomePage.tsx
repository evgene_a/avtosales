import React, { FC } from 'react';
import { useSelector } from 'react-redux';
import { QuestionForm } from '../forms';
import { ListQuestions, Layout } from '../components';
import { Selectors } from '../store';
import { filterData } from '../service';

export const HomePage: FC = () => {
  const stateData = useSelector(Selectors.userMessage);
  const search = useSelector(Selectors.searchMessage);
  return (
    <Layout>
      <ListQuestions data={filterData(stateData, search)} />
      <QuestionForm />
    </Layout>
  );
};
