import { userDateState } from '../data';
import {
  toggleIsUserMessage,
  toggleIsUserData,
  deleteUserData,
  editUserData,
  UserMessage,
  typeOnLoader,
  onLoader,
  dataError,
  typeDataError,
  ActionsType,
  ActionsTypeData,
  deleteTypeData,
  editTypeData,
} from '../store';

const url = new URL(String(process.env.REACT_APP_SERVER_URL));

export const postData = (userData: UserMessage) => {
  url.pathname = 'posts';
  return (dispatch: (arg: ActionsType | typeDataError) => void) => {
    fetch(url.href, {
      method: 'POST',
      body: JSON.stringify({
        userData,
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    })
      .then((response) => response.json())
      .then((json) => {
        dispatch(toggleIsUserMessage(json.userData));
      })
      .catch((err) => dispatch(dataError(String(err))));
  };
};

export const getData = () => {
  return (
    dispatch: (arg: ActionsTypeData | typeOnLoader | typeDataError) => void
  ) => {
    try {
      dispatch(onLoader());
      setTimeout(() => dispatch(toggleIsUserData(userDateState)), 2000);
    } catch (err) {
      dispatch(dataError(String(err)));
    }
  };
};

export const deleteData = (id: string) => {
  url.pathname = `posts/${id}`;
  return (dispatch: (arg: deleteTypeData | typeDataError) => void) => {
    fetch(url.href, {
      method: 'DELETE',
    })
      .then((res) => {
        if (res.status >= 200 && res.status < 400) {
          dispatch(deleteUserData(id));
        }
      })
      .catch((err) => dispatch(dataError(String(err))));
  };
};

export const putData = (userMessage: UserMessage) => {
  url.pathname = `posts/1`;
  return (
    dispatch: (arg: editTypeData | typeOnLoader | typeDataError) => void
  ) => {
    dispatch(onLoader());
    fetch(url.href, {
      method: 'PUT',
      body: JSON.stringify(userMessage),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    })
      .then((res) => res.json())
      .then((json) => {
        json.id = userMessage.id;
        dispatch(editUserData(json));
      })
      .catch((err) => dispatch(dataError(String(err))));
  };
};
