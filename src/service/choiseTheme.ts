interface ITheme {
  blueTheme: { [key: string]: string };
  darkTheme: { [key: string]: string };
}

export const choiseTheme = (check: boolean, Theme: ITheme) => {
  const { blueTheme, darkTheme } = Theme;
  switch (check) {
    case true:
      return darkTheme;
    default:
      return blueTheme;
  }
};
