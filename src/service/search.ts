import { TypeUserMessages } from '../store';

export const filterData = (data: TypeUserMessages, search: string) => {
  const SearchWord = search.toLowerCase();
  if (SearchWord) {
    const searchArray = [...data].filter(
      (item) =>
        item.text.toLowerCase().includes(SearchWord) ||
        item.carBrand.toLowerCase().includes(SearchWord) ||
        item.carModel.toLowerCase().includes(SearchWord)
    );
    if (searchArray.length) return searchArray;
    return [];
  }
  return data;
};
