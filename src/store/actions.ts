/* eslint-disable import/no-cycle */
import { ACTIONS_TYPE } from './constants';
import { TypeUserMessages, UserMessage } from './reducers';

export const toggleIsUserMessage = (userMessage: UserMessage) =>
  ({
    type: ACTIONS_TYPE.USERS_MESSAGES,
    payload: { userMessage },
  } as const);
export type ActionsType = ReturnType<typeof toggleIsUserMessage>;

export const toggleIsUserData = (userMessage: TypeUserMessages) =>
  ({
    type: ACTIONS_TYPE.GET_USERDATA,
    payload: { userMessage },
  } as const);
export type ActionsTypeData = ReturnType<typeof toggleIsUserData>;

export const deleteUserData = (id: string) =>
  ({
    type: ACTIONS_TYPE.DELETE_MESSAGE,
    payload: { id },
  } as const);
export type deleteTypeData = ReturnType<typeof deleteUserData>;

export const editUserData = (userMessage: UserMessage) =>
  ({
    type: ACTIONS_TYPE.EDIT_MESSAGE,
    payload: { userMessage },
  } as const);
export type editTypeData = ReturnType<typeof editUserData>;

export const themeCheked = (themeCheck: boolean) =>
  ({
    type: ACTIONS_TYPE.CHECK_THEME,
    payload: { themeCheck },
  } as const);
export type typeCheckTheme = ReturnType<typeof themeCheked>;

export const onLoader = () =>
  ({
    type: ACTIONS_TYPE.ON_LOADER,
  } as const);

export type typeOnLoader = ReturnType<typeof onLoader>;

export const dataError = (error: string) =>
  ({
    type: ACTIONS_TYPE.ERROR_MESSAGE,
    payload: { error },
  } as const);
export type typeDataError = ReturnType<typeof dataError>;

export const searchUserMessages = (search: string) =>
  ({
    type: ACTIONS_TYPE.SEARCH_MESSAGE,
    payload: { search },
  } as const);
export type typeSearchUserMessages = ReturnType<typeof searchUserMessages>;
