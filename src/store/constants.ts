export const enum ACTIONS_TYPE {
  USERS_MESSAGES = 'auto/users/USERS_MESSAGES',
  GET_USERDATA = 'auto/users/GET_USERDATA',
  DELETE_MESSAGE = 'auto/users/DELETE_MESSAGE',
  EDIT_MESSAGE = 'auto/users/EDIT_MESSAGE',
  CHECK_THEME = 'auto/users/CHECK_THEME',
  ON_LOADER = 'auto/users/ON_LOADER',
  ERROR_MESSAGE = 'auto/users/ERROR_MESSAGE',
  SEARCH_MESSAGE = 'auto/users/SEARCH_MESSAGE',
}
