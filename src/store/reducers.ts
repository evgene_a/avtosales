/* eslint-disable import/no-cycle */
import { ACTIONS_TYPE } from './constants';
import {
  ActionsType,
  ActionsTypeData,
  deleteTypeData,
  editTypeData,
  typeCheckTheme,
  typeDataError,
  typeOnLoader,
  typeSearchUserMessages,
} from './actions';

export type TypeUserMessages = UserMessage[];

export type UserMessage = {
  id: string;
  time: string;
  email: string;
  text: string;
  carModel: string;
  carBrand: string;
  name: string;
};

const initialState = {
  loading: false,
  error: '',
  userMessages: [] as TypeUserMessages,
  themeCheck: false,
  searchMessage: '',
};

type UsersType = typeof initialState;

export const usersMessagesReducer = (
  state: UsersType = initialState,
  action:
    | ActionsType
    | ActionsTypeData
    | deleteTypeData
    | editTypeData
    | typeCheckTheme
    | typeOnLoader
    | typeDataError
    | typeSearchUserMessages
): UsersType => {
  switch (action.type) {
    case ACTIONS_TYPE.USERS_MESSAGES: {
      return {
        ...state,
        loading: false,
        userMessages: [...state.userMessages, action.payload.userMessage],
      };
    }
    case ACTIONS_TYPE.ON_LOADER: {
      return {
        ...state,
        loading: true,
      };
    }
    case ACTIONS_TYPE.GET_USERDATA: {
      return {
        ...state,
        loading: false,
        userMessages: action.payload.userMessage,
      };
    }
    case ACTIONS_TYPE.SEARCH_MESSAGE: {
      return {
        ...state,
        searchMessage: action.payload.search,
      };
    }
    case ACTIONS_TYPE.ERROR_MESSAGE: {
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      };
    }
    case ACTIONS_TYPE.DELETE_MESSAGE: {
      return {
        ...state,
        loading: false,
        userMessages: [
          ...state.userMessages.filter((item) => item.id !== action.payload.id),
        ],
      };
    }
    case ACTIONS_TYPE.EDIT_MESSAGE: {
      return {
        ...state,
        loading: false,
        userMessages: [
          ...state.userMessages.map((item) =>
            item.id === action.payload.userMessage.id
              ? action.payload.userMessage
              : item
          ),
        ],
      };
    }
    case ACTIONS_TYPE.CHECK_THEME: {
      return {
        ...state,
        themeCheck: action.payload.themeCheck,
      };
    }
    default:
      return state;
  }
};
