import { AppStateType } from './store';

export const userMessage = (state: AppStateType) =>
  state.userMessages.userMessages;
export const themeValue = (state: AppStateType) =>
  state.userMessages.themeCheck;
export const loaderCheck = (state: AppStateType) => state.userMessages.loading;
export const loadError = (state: AppStateType) => state.userMessages.error;
export const searchMessage = (state: AppStateType) =>
  state.userMessages.searchMessage;
