import { combineReducers, createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { usersMessagesReducer } from './reducers';

const reducer = combineReducers({
  userMessages: usersMessagesReducer,
});

export type RootReducerType = typeof reducer;
export type AppStateType = ReturnType<RootReducerType>;

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));
export default store;
