import styled from '@emotion/styled';

const SFooter = styled.footer`
  @media screen and (max-width: 599px) {
    .MuiPaper-root {
      display: flex;
      flex-direction: column-reverse;
      justify-content: center;
    }
    h5 {
      padding-top: 20px;
    }
  }
`;

export { SFooter };
