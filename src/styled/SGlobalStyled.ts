import { createGlobalStyle } from 'styled-components';

interface IGlobalProps {
  background: string;
}

export const GlobalStylesAllPages = createGlobalStyle<IGlobalProps>`
  * {
    margin: 0;
    padding: 0;
  }

  body {
    font-family: Roboto, -apple-system, BlinkMacSystemFont, 'Segoe UI', Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    height: 100%;
    background-color: ${({ background }) => background};
  }
`;
