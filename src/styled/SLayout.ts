import styled from '@emotion/styled';

const SLayout = styled.div`
  main {
    min-height: 80.3vh;
    flex-grow: 1;
    padding-bottom: 35px;
  }
  .css-c19kj9 {
    padding: 0 15px;
  }

  @media screen and (max-width: 635px) {
    .css-yfpj34-MuiPaper-root {
      padding: 1px 0 70px 0;
    }
    .css-1328vbt-MuiPaper-root {
      padding: 1px 0 70px 0;
    }
    .css-11paif2,
    .css-116z537 {
      margin: 0 auto;
      padding: 0 20px;
    }
  }
`;

export { SLayout };
