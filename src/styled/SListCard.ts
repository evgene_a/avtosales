import styled from '@emotion/styled';

const CardUserQuestions = styled.li`
  font-size: 14px;
  list-style-type: none;
  margin-bottom: 35px;
  position: relative;
`;

const BlockUserData = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const UserName = styled.div`
  margin-right: 15px;
  font-weight: bold;
  font-size: 16px;
`;

const UserdataAuto = styled.div`
  display: flex;
  flex-wrap: wrap;
  div {
    margin-right: 4px;
    font-weight: bold;
    font-size: 16px;
    color: brown;
  }
`;
const BlockText = styled.div`
  line-height: 1.5;
  padding: 0 10px;
`;

const Time = styled.div`
  font-size: 13px;
  padding-bottom: 15px;
  color: rgb(133, 133, 133);
`;

export {
  CardUserQuestions,
  BlockUserData,
  UserName,
  UserdataAuto,
  BlockText,
  Time,
};
