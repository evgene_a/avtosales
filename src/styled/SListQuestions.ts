import styled from '@emotion/styled';

export const SListQuestions = styled.div`
  padding-bottom: 40px;
  ul {
    margin: 0;
    padding: 0;
  }
`;
