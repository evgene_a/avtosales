import styled from '@emotion/styled';
import { Dialog } from '@mui/material';

export const Content = styled(Dialog)`
  .MuiPaper-root {
    border-radius: 10px;
    width: 525px;
    margin: 15px;
  }
`;
