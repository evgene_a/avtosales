import styled from '@emotion/styled';

interface ISQuestionForm {
  textcolor: string;
  backgroundinput?: string;
  backgroundtextarea?: string;
  areaborder?: string;
}

const SQuestionForm = styled.div<ISQuestionForm>`
  max-width: 460px;
  padding-left: 15px;

  div {
    font-size: 13px;
  }

  .MuiChip-label {
    color: ${({ textcolor }) => textcolor};
    font-size: 14px;
    font-weight: 600;
    width: 150px;
  }

  form.div {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }

  h1 {
    font-weight: 100;
    color: white;
    text-align: center;
    padding-bottom: 10px;
    border-bottom: 1px solid rgb(79, 98, 148);
  }
  form {
    max-width: 460px;
  }

  p {
    color: red;
    font-size: 12px;
    margin: 0 0 10px 0;
  }

  p::before {
    display: inline;
    content: '⚠ ';
  }

  input {
    display: block;
    box-sizing: border-box;
    border-radius: 5px;
    padding: 10px 15px;
    font-size: 14px;
    height: 25px;
    margin: 8px;
    width: 93%;
    color: black;
    background: ${({ backgroundinput }) => backgroundinput};
  }

  textarea {
    border-radius: 5px;
    border: 1px solid ${({ areaborder }) => areaborder};
    width: 96%;
    margin-top: 10px;
    height: 80px;
    padding: 8px;
    resize: none;
    background: ${({ backgroundtextarea }) => backgroundtextarea};
  }

  label {
    line-height: 2;
    text-align: left;
    display: block;
    margin-bottom: 13px;
    margin-top: 20px;
    color: white;
    font-size: 14px;
    font-weight: 200;
  }

  input[type='submit'] {
    color: white;
    text-transform: uppercase;
    border: 1px;
    margin-top: 40px;
    padding: 20px;
    font-size: 16px;
    font-weight: 100;
    letter-spacing: 10px;
  }

  input[type='submit']:hover {
    background: #bf1650;
  }

  button[type='submit']:active,
  input[type='button']:active,
  input[type='submit']:active {
    transition: 0.3s all;
    transform: translateY(3px);
    border: 1px solid transparent;
    opacity: 0.8;
  }

  input:disabled {
    opacity: 0.4;
  }

  input[type='button']:hover {
    transition: 0.3s all;
  }

  input[type='button'],
  input[type='submit'] {
    -webkit-appearance: none;
  }
`;

const InputsBlock = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  flex-wrap: wrap;
  width: 100%;
  div {
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    margin-bottom: 5px;
  }
  @media screen and (max-width: 635px) {
    flex-direction: column;
    justify-content: space-between;
  }
`;

interface ISQuestionButton {
  buttonhover?: string;
}

const Button = styled.div<ISQuestionButton>`
  position: relative;
  Button {
    position: absolute;
    margin: 8px;
    right: 0;
  }
  Button:hover {
    background-color: ${({ buttonhover }) => buttonhover};
    box-shadow: 0px 2px 4px -1px rgb(0 0 0 / 20%),
      0px 4px 5px 0px rgb(0 0 0 / 14%), 0px 1px 10px 0px rgb(0 0 0 / 12%);
  }
`;

export { SQuestionForm, InputsBlock, Button };
