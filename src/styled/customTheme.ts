import { createTheme } from '@mui/material';

export const defaultTheme = (allColors: any) =>
  createTheme({
    palette: {
      success: {
        main: allColors.primaryColorTheme,
        contrastText: '#fff',
        light: allColors.textColorFooter,
        dark: allColors.layoutColor,
      },
      primary: {
        main: allColors.backgroundColor,
        light: allColors.textColorFooter,
        dark: allColors.cardBackgroundColor,
        contrastText: '#fff',
      },
      warning: {
        main: allColors.colorSwitch,
        light: allColors.colorBorderInput,
      },
      background: {
        paper: allColors.primaryColorTheme,
      },
    },

    components: {
      MuiAppBar: {
        styleOverrides: {
          root: {
            boxShadow: 'none',
            background: allColors.primaryColorTheme,
            marginBottom: '30px',
            fontSize: '0.7em',
          },
        },
      },
    },
  });
