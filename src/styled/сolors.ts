const ThemeColors = {
  blueTheme: {
    primaryColorTheme: '#5181b8',
    backgroundColor: '#EEEEEE',
    textColorFooter: 'white',
    layoutColor: '#f5f5f5',
    cardBackgroundColor: '#E8E8E8',
    colorSwitch: '#63B0F7',
    colorBorderInput: 'rgb(183,183,183)',
  },
  darkTheme: {
    primaryColorTheme: '#363636',
    backgroundColor: '#818181',
    textColorFooter: 'white',
    layoutColor: '#6b6b6b',
    cardBackgroundColor: '#aeaeae',
    colorSwitch: '#7b7b7b',
    colorBorderInput: '#565656',
  },
};

export { ThemeColors };
