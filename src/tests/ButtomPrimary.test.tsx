import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ButtonPrimary } from '../components';

const onClick = jest.fn();

describe('Test Button component', () => {
  test('onClick button', () => {
    const { getByTestId } = render(
      <ButtonPrimary data-testid="simple-button" onClick={onClick} />
    );
    const button = getByTestId('simple-button');
    userEvent.click(button);
    expect(onClick).toBeCalled();
  });
});
