import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import { Header } from '../components';
import { MESSAGES } from '../constants';

test('search header title', () => {
  render(<Header />);
  const headerGetText = screen.getByText(MESSAGES.header.headerTitle);
  expect(headerGetText).toBeInTheDocument();
});
