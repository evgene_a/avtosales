import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import { ListQuestions } from '../components';
import { MESSAGES } from '../constants';

const nullArray = [] as const;
const data = [
  {
    id: '123',
    time: '12:30',
    email: 'proverka@mail.ru',
    text: 'text',
    carModel: 'any',
    carBrand: 'any',
    name: 'my name',
  },
];

describe('Test ListQuestions component', () => {
  test('Component render', () => {
    render(<ListQuestions data={data} />);
    expect(screen.getByText(/12:30/i)).toBeInTheDocument();
  });
  test('Null render', () => {
    // @ts-ignore
    render(<ListQuestions data={nullArray} />);
    expect(screen.getByText(MESSAGES.ListQuestion)).toBeInTheDocument();
  });
});
