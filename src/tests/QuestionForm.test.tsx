import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import userEvent from '@testing-library/user-event';
import { QuestionForm } from '../forms';
import { MESSAGES } from '../constants';
import store from '../store/store';

describe('Test QuestionForm component', () => {
  test('Form render', () => {
    render(
      <Provider store={store}>
        <QuestionForm />
      </Provider>
    );
    expect(
      screen.queryByPlaceholderText(MESSAGES.questionForm.nameForm.placeholder)
    ).toBeInTheDocument();
  });

  test('Input focus', () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <QuestionForm />
      </Provider>
    );
    const input = getByTestId('input-name');
    userEvent.click(input);
    expect(input).toHaveFocus();
  });

  test('Button disable', () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <QuestionForm />
      </Provider>
    );
    expect(getByTestId('button')).toBeDisabled();
  });

  test('Input-name have red style', async () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <QuestionForm />
      </Provider>
    );
    const name = getByTestId('input-name');
    const email = getByTestId('email');
    userEvent.type(name, 'test');
    userEvent.click(email);
    const inputName = await screen.findByTestId('input-name');
    expect(inputName).toHaveStyle({
      border: '1px solid red',
    });
  });

  test('Input-name have usually style', async () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <QuestionForm />
      </Provider>
    );
    const name = getByTestId('input-name');
    userEvent.type(name, 'test');
    const inputName = await screen.findByTestId('input-name');
    expect(inputName).toHaveStyle({
      border: '1px solid rgb(133, 133, 133)',
    });
  });
});
