import {
  toggleIsUserMessage,
  UserMessage,
  usersMessagesReducer,
} from '../store';

let startState = {
  loading: false,
  error: '',
  userMessages: [] as UserMessage[],
  themeCheck: false,
  searchMessage: '',
};

beforeEach(() => {
  startState = {
    userMessages: [
      {
        name: 'Евгений',
        email: 'evgene.fe@gmail.com',
        carBrand: 'Reno',
        carModel: 'Megan',
        text: 'Lorem ipsum dolor sit amet, consecrate disciplining elit. Aliquam at ipsum nisl. Pellentesque facilisis mi metus, eget tempus dui pellentesque id. Duis condimentum varius enim a sollicitudin. Mauris ditherer solicitudes mass ac exposure. Null oculist ut magna eu condiment. Crash a exists rises, et actor nun. Nam critique mollies libero, vitae modernism libero aliquot et. Pron sed nil ut erat mollies condiment. Dis incident libero nec actor facilitates.',
        time: '16.03.2022 в 10:08',
        id: 'R6P3qGNt2H0Er9LPp4t55y',
      },
      {
        name: 'Евгений',
        email: 'evgene.fe@gmail.com',
        carBrand: 'Reno',
        carModel: 'Megan',
        text: 'Lorem ipsum dolor sit amet, consecrate disciplining elit. Aliquam at ipsum nisl. Pellentesque facilisis mi metus, eget tempus dui pellentesque id. Duis condimentum varius enim a sollicitudin. Mauris ditherer solicitudes mass ac exposure. Null oculist ut magna eu condiment. Crash a exists rises, et actor nun. Nam critique mollies libero, vitae modernism libero aliquot et. Pron sed nil ut erat mollies condiment. Dis incident libero nec actor facilitates.',
        time: '15.03.2022 в 12:08',
        id: 'R6P3qGNtg766fhfghEr9LPp4t4y',
      },
    ],
    themeCheck: false,
    loading: false,
    error: '',
    searchMessage: '',
  };
});


test('correct reducer should be added', () => {
  const userMessage: UserMessage = {
    name: 'Саша',
    email: 'evgene.fe@gmail.com',
    carBrand: 'Reno',
    carModel: 'Megan',
    text: 'Lorem ipsum dolor sit amet, consecrate disciplining elit. Aliquam at ipsum nisl. Pellentesque facilisis mi metus, eget tempus dui pellentesque id. Duis condimentum varius enim a sollicitudin. Mauris ditherer solicitudes mass ac exposure. Null oculist ut magna eu condiment. Crash a exists rises, et actor nun. Nam critique mollies libero, vitae modernism libero aliquot et. Pron sed nil ut erat mollies condiment. Dis incident libero nec actor facilitates.',
    time: '15.03.2022 в 12:08',
    id: 'R6P3qGNtg766f665767hfghEr9LPp4t4y',
  };

  const endState = usersMessagesReducer(
    startState,
    toggleIsUserMessage(userMessage)
  );

  expect(endState.userMessages[2].name).toBe('Саша');
});
